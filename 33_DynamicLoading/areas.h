#ifndef __AREAS_H__
#define __AREAS_H__

#ifdef __cplusplus
extern "C" {
#endif

    const char **catalogo();
    int cir (int radio);
    int cua (int base);
    int tri (int base, int altura);

#ifdef __cplusplus
}
#endif

#endif

