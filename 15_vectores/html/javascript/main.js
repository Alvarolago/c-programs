/* Canvas variables */
var pen,
    C_WIDTH,
    C_HEIGHT,
    XC,
    YC


/* Window variables */
var scale = 500

/* Curves */
var profile, tan, nor

/* Translate from window to canvas coordinates */
function X(x) { return XC + scale * x }
function Y(y) { return YC - scale * y }

function mark(p) {
    pen.arc(X(p[0]), Y(p[1]), 4, 0, 2 * Math.PI)
}

function view(p) {
    /* Lay a line between points */
    pen.beginPath()
    pen.strokeStyle = "#3333FF"
    pen.lineWidth = 2
    pen.moveTo(X(p[0][0]), Y(p[0][1]))
    for (var i=1; i<p.length; i++)
        pen.lineTo(X(p[i][0]), Y(p[i][1]))
    pen.stroke()
    pen.lineWidth = 1

    /* Set a mark on each and every point */
    for (var i=0; i<p.length; i++){
        pen.beginPath()
        pen.fillStyle = "#CCCCFF"
        mark(p[i])
        pen.fill()
    }

}

function view_in_place(where, v, color) {
    pen.beginPath()
    pen.strokeStyle = color
    for (var i=0; i<v.length; i++){
        pen.moveTo( X(where[i][0]), Y(where[i][1]) )
        pen.lineTo( X(where[i][0] + v[i][0]), Y(where[i][1] + v[i][1]) )
    }
    pen.stroke()
}

function draw(){
    view_in_place(profile.transform("p"), tan.transform("p"), "blue")   /* View al tangent vector in its point*/
    view_in_place(profile.transform("p"), nor.transform("p"), "red")   /* View al normal vector in its point*/
    view(profile.transform("p"))          /* Original curve */
}

function module(v){
    return Math.sqrt(v[0] * v[0] + v[1] * v[1])
}

function main(){
    /* Initialize graphic data */
    var c = document.getElementById("lienzo")
    C_WIDTH  = c.width
    C_HEIGHT = c.height
    XC       = C_WIDTH / 2
    YC       = C_HEIGHT / 2
    pen      = c.getContext("2d")

    /* Data */
    profile = new Curve(point)
    profile.scale = 1
    profile.digest()

    tan = new Curve(profile.u)
    tan.scale = 0.5
    tan.digest()

    nor = new Curve(tan.u)
    nor.scale = 0.05
    nor.digest()


    draw()
}
