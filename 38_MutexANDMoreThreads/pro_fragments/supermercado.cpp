//Compilar con cc -o supermercado supermercado.cpp -lpthread
#include <stdio.h>
#include <pthread.h>
#include <strings.h>

#include "stack.h"

void menu(){
    printf("-----MENU-----\n");
    printf("1.-Crear Hilo\n");
    printf("2.-Cepillar Hilo\n");
    printf("3.-Cerrar Hilo\n");
}

struct showid {
    int *id;
    int count;
};


void* show_thread_id(void *arg){
    FILE *pFile;
    long id = *((long*) arg);
    pFile = fopen("fichero.txt","w");
    fprintf(pFile, "%li", id);
    fclose(pFile);
    return NULL;
}

long createThread(pthread_t thread_id){
    struct showid thread_args;
    pthread_create (&thread_id, NULL, &show_thread_id, &thread_id);

    printf("Se ha creado el hilo %li\n", thread_id);
  return thread_id;
}

void deleteThread(long id){

      pthread_cancel (id);
      printf("Se ha eliminado el hilo %li\n", id);
}


void closeThreads(pthread_t thread_id){
    pthread_join (thread_id, NULL);
    printf("Se ha cerrado el hilo %li\n", thread_id);
}

int main(){
    struct TStack stack;
    bzero (&stack, sizeof(stack));

    int opcion;
    pthread_t thread_id;

    do{
        menu();
        scanf("%i", &opcion);
        switch ( opcion ){
            case 1:
                thread_id = createThread(thread_id);
                push(&stack, thread_id);      //Metemos en la pila la id del hilo creado
               break;
            case 2:
                if(stack.climb = 0){
                    printf("La pila está vacia.\n");
                }else{
                    thread_id = pop(&stack);
                    //deleteThread(thread_id);
                }
                break;
            case 3:
                for(int cnt = 0; cnt < stack.climb; cnt++){
                    closeThreads(stack.data[cnt]);
                }
                break;
        }

    }while(opcion != 4);


    return 0;
}
