#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){

    int vector_Y, vector_X, vector_Z;
    int modulo;
    double resultado;

    printf("Indique un vector(X,Y,Z): ");
    scanf("%i %i %i", &vector_X, &vector_Y, &vector_Z);

    modulo = (vector_X*vector_X) + (vector_Y*vector_Y) +(vector_Z*vector_Z);
    resultado = sqrt(modulo);

    printf("El modulo es: %f\n", resultado);
    return EXIT_SUCCESS;

}

