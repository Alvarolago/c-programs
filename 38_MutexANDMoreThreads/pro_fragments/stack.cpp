#include "stack.h"

void push (struct TStack *p, long newId){
     p->data[p->climb] = newId;
     p->climb++;
}

int pop (struct TStack *p){
     int data;
     p-> climb--;
     data = p->data[p->climb];

     return data;
}

