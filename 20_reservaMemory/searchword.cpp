#include <strings.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define FILENAME "buscar.txt"
#define N 0x100

//Función punto de entrada
int main(){

    long inicio, fin, longfile;
    FILE *pf;
    char *texto;
    int i = 0;
    char palabra[N];
    int longword = 0;
    int w = 0;

    if( !(pf= fopen(FILENAME, "rb")) ){
        fprintf(stderr, "No he podido abrir el archivo");
        return EXIT_FAILURE;
    }


    //Tamaño del texto
    inicio = ftell(pf);
    fseek(pf, 0, SEEK_END);
    fin = ftell(pf);
    rewind (pf);
    longfile = fin - inicio;

    texto = (char *) malloc(longfile);
    fread(texto, sizeof(char), longfile, pf);


    //Ask word to search
    printf ("Palabra para buscar: ");
    scanf (" %s", palabra);


    //Size of the word

    while(palabra[i++] != '\0')       //Igual que strlen(palabra);
	    longword++;


    printf("Tu palabra es: %s\n", palabra); 		 		  //Print word search
    printf("Tu palabra mide %i caracteres\n", longword);	          //Print length of word search
    printf("\n");


    for(int i=0; i < longfile; i++)                                //Print text from file
        printf("%c", texto[i]);

  
    printf("Your text has %li characters\n", longfile);		  //Print lenght of file
    printf("\n\n");


    //Algoritm to search word
    for(int cnt=0; cnt<longfile; cnt++){
         if(palabra[w] == texto[cnt]){
	    ++w;
	    if(w == longword)
		    printf("Tu palabra comienza en el caracter: %i\n", 2 + cnt- w);
	}
	else w=0;
    }

    //Set free the memory
    free(texto);
    fclose(pf);

    return EXIT_SUCCESS;
}
