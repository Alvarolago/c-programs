#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#define ARCH "macbeth.txt"

int main(int argc, char *argv[]){

	long inicio, fin;	//Se restan para calcular d
	long d;                	//d es la longitud
	char *texto;		//Puntero al malloc

	FILE *pf;		//Puntero al archivo pf

	if ( !(pf = fopen(ARCH, "rb")) ){
		fprintf(stderr, "Error\n");
		return EXIT_FAILURE;
	}

	//De esta forma calculamos el inicio y el fin del fechero, así conocemos su medida
	inicio = ftell(pf);
	fseek(pf, 0, SEEK_END);
	fin = ftell(pf);

	d = fin - inicio; 

	texto = (char*) malloc(d+1);		//Malloc, es un array en el disco duro

	fread (texto, sizeof(char),d ,pf );	//con fread leemos el archivo


	free(texto);				//Liberamos la memoria del malloc
	fclose(pf);				//Cerramos el archivo


    return EXIT_SUCCESS;

}

