#ifndef __STAK_H__
#define __STAK_H__

#include <pthread.h>

struct TStack {
    int climb;
    long int data[100];
};

#ifndef __cplusplus
extern "C"
{
#endif

    void push (struct TStack *p, long newId);
    void pop  (struct TStack *p);

#ifdef __cplusplus
}
#endif

#endif
