#include <stdio.h>
#include <stdlib.h>
#include "general.h"

#define MAX 0x100
#define N 0x100


int menu(){
        char respuesta;
        printf("Continuar?");
        scanf("%c",&respuesta);
        if(respuesta == 's')
                return 1;
        else{
                if(respuesta == 'n')
                        return 0;
        }
}


void brillo(){
        int  brillo;
        printf("Indique el brillo (0-6): ");
        scanf("%i", &brillo);
        if(brillo<8){
                printf("Indique un numero entero valido entre 0 y 6");
        }
}



void push (struct TPila *p, struct TEstrella *nuevo){
        p->datos[p->cima] = nuevo;
        p->cima++;
}

void preguntar(struct TPila *stack, struct TEstrella *datos, FILE *pf){

        int brillo;
        int seguir;
        int nuevo = 0;


                do{
                        datos = (TEstrella *)malloc(MAX);

                        printf("Indique la coordenada X: ");
                        scanf("%lf", &datos->coordX);

                        printf("Indique la coordenada Y: ");
                        scanf("%lf", &datos-> coordY);

                        printf("Indique el diametro: ");
                        scanf("%lf", &datos-> diametro);

                        printf("Indique la distancia: ");
                        scanf("%lf", &datos-> distancia);

                        printf("Indique la masa: ");
                        scanf("%lf", &datos-> masa);

                        printf("Indique la edad: ");
                        scanf("%lf", &datos-> edad);


                        menu();
                        seguir = menu();

                        push(stack, datos);
                        fwrite(datos, sizeof(char), sizeof(datos), pf);

                }while(seguir);

                free(datos);
}

