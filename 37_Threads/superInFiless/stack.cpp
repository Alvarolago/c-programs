#include "stack.h"

void push(struct TStack *p, long newId){
    p->data[p->summit] = newId;
    p->summit++;
}

int pop(struct TStack *p){
    int data;
    p->summit--;
    data = p->data[p->summit];

    return data;
}
