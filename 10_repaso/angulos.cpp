#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define DIM 2
#define PI 3.14

int  main(){

    double vec[DIM],
           modulo;
    double tan, angulo;

    system ("clear");

    printf ("Vector: ");
    scanf (" %lf, %lf", &vec[0], &vec[1]);

    tan=vec[1]/vec[0];
    printf("Tangente es: %f\n",tan);

    angulo = atan(tan) * 180/ PI;
    printf("Angulo es %f\n", angulo);


    return EXIT_SUCCESS;
}

