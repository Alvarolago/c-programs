#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include "extend.cpp"
#include "general.h"

#define FILENAME "fichero.txt"
#define MAX 0x100

#define N 0x100 		//Cantidad de estrellas que caben


extern const struct TEstrella *estrella;
extern const struct TPila *stack;


int main(int argc, char *argv[]){

    struct TEstrella *estrella;
    struct TPila *stack = NULL;

    FILE *pf;
    char respuesta;
    int seguir;
 
    bzero (&stack, sizeof(stack));

	if( !(pf = fopen(FILENAME, "w")) ){
        	fprintf(stderr, "Error No es posible abrir el archivo indicado\n");
	        return EXIT_FAILURE;
    	}
	
	preguntar(stack, estrella, pf);

	fclose(pf);  

    return EXIT_SUCCESS;

}

