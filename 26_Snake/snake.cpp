#include "snake.h"
#include <strings.h>

#define POSIB 4
#define AINI  5

int posib;

int iniciado = 0;
int max_x = 80,
    max_y = 25;

const struct TVector velocidades[POSIB] = {
    { 0, 1},
    { 0,-1},
    {-1, 0},
    { 1, 0}
};

void iniciar ( int lines, int cols ){
    srand (time(NULL));     //srand dice por que numero comienza y time nos da el numero de segundos(Crear num random)
    iniciado = 1;
    max_x = cols;
    max_y = lines;

}


void parir (struct TSnake *snake){
    bzero (snake, sizeof(struct TSnake));
    snake->anillo[0].pos.x = 0;		//(rand () % (100 * max_x) / 100.);
    snake->anillo[0].pos.y = 0;		//(rand () % (100 * max_x) / 100.);

    snake->anillo[0].vel = velocidades [posib];
    snake->cima++;
    for(int i=0; i<AINI; i++)
        crecer (snake);
}

void mover  (struct TSnake *snake){
    for(int i=snake->cima-1; i>0; i--)
        snake->anillo[i] = snake->anillo[i-1];

    snake->anillo[0].pos.x += snake->anillo[0].vel.x;
    snake->anillo[0].pos.y += snake->anillo[0].vel.y;
}




void crecer (struct TSnake *snake) {
    if(snake->cima >= AMAX)
        return;
    snake->anillo[snake->cima].pos = snake->anillo[snake->cima-1].pos;    //Pos x y igual que la del anillo anterior.
    snake->cima++;
}
