#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

#define MAX 5

struct TPila{
	int dato[MAX];
	int cima;
};

void push(struct TPila *p,int nuevo){
	p->dato[p->cima] = nuevo;
	p-> cima++;
}

void pop(struct TPila *p){
	int dato;
	p->cima--;
	dato = p->dato[p->cima];

//	return dato;
}

int main(int argc, char *argv[]){
	struct TPila pila;
	int nuevo;
	bzero(&pila,sizeof(pila));

	int opcion;
	do{
		printf("Indique que acción desea realizar(0-Salir) (1-Introducir) (2-Sacar)");
		scanf("%i", &opcion);
		switch(opcion){
		case 1:
			scanf("%i", &nuevo);
			push(&pila,nuevo);
			for(int cnt=0; cnt<MAX; cnt++)
				printf("%i\n", pila.dato[cnt]);
		break;
		case 2:
			pop(&pila);
			for(int cnt=0; cnt<MAX; cnt++)
				printf("%i\n", pila.dato[cnt]);
		}

	}while(opcion != 0);
	
	return EXIT_SUCCESS;
}
