#include "areas.h"




const char *names[] = {
    "cir",
    "cua",
    "tri",
    (char *) 0
};

const char **catalogo () { return names; }

int cir (int radio){ return 3.14 * radio * radio; }
int cua (int base){ return base * base; }
int tri (int base, int altura){ return (base * altura) / 2; }
