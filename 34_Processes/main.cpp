#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>


int spawn(char *comando, char *lc []){    //Pasamos el comando de terminal y los parámetros
    pid_t child_pid;

    child_pid = fork();

    if(child_pid != 0){
        /*Soy el padre*/
        return child_pid;
    }else{
        /*Soy el hijo*/
        execvp(comando, lc);            //Solo retorna si ocurre un error
        printf("Error");
        abort();
    }
}



int main (int agrc, char *argv[]){

    pid_t child_pid;
    int child_status;
    int resultado;

    child_pid = spawn("./suma", argv);

    wait(&child_status);

    if(WIFEXITED (child_status)){
        resultado = WEXITSTATUS(child_status);    //WEXITSTATUS coge le valor de retorno del hijo
        printf("La suma vale %i\n", resultado);
    }
    else
        printf("child process exited abnormally\n");



    return EXIT_SUCCESS;
}
