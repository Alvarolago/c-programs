#include <string>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>


int main(int argc, char *argv[]){


	int width = 640;
	int height = 480;

	int x = width / 2, y = height / 2;


	int curlFrame = 0;
	int frameWidth = 128;
	int frameHeight = 1;

    //Allegro variable
    ALLEGRO_DISPLAY *display = NULL;
    ALLEGRO_BITMAP *imagen = NULL;

    //init programam
    if(!al_init())
	    return EXIT_FAILURE;

    al_init_image_addon();

    //Create
    display = al_create_display(width, height);
    imagen = al_load_bitmap("character.jpg");

    //al_clear_to_color(al_map_rgb(0,0,255));

    //al_draw_bitmap(imagen,200,200,0);

    al_flip_display();

    al_rest(10.0);


    //Display objetct
    al_destroy_bitmap(imagen);
    al_destroy_display(display);



    return EXIT_SUCCESS;
}
