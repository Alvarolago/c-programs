#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define NUM_SIGNALS 3

const char *program = "./hijo";

int spawn () {
    pid_t child_pid;
    child_pid = fork();  /*Creamos el fork para dividir el programa en 2 procesos
                          iguales con distinto valor de retorno*/
    if(child_pid != 0)
        return child_pid;
     //Else
     execvp (program, NULL); /*Arrancamos el proceso hijo en otro archivo "hijo.cpp"*/
     fprintf (stderr, "Un error ocurrio en execvp\n");
     abort ();

}


int main() {
    pid_t pid_child;

    int child_status;

    pid_child = spawn();  /*Guardamos el identificador del hijo que retorna spawn
                                                   y a la vez llamamos a la función*/

    for(int cnt = 0; cnt < NUM_SIGNALS; cnt++){  /*Mandamos 3 señales con un delay de 1 segundo*/
    	kill(pid_child, SIGUSR1);
    	usleep(1000000);
        printf("%i señales enviadas\n", cnt);
    }

    wait(&child_status);    /*Esperamos a que el hijo acabe para posteriormente
                            limpiarlo y evitar que se quede Zombie*/

    if(!WIFEXITED (child_status))
        printf("child process exited abnormally\n");

    return EXIT_SUCCESS;
}
