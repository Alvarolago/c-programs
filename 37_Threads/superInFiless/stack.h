#ifndef __STACK_H__
#define __STACK_H__

#define MAX 0x100

struct TStack {
    int summit = 0;
    int data[MAX];
};

#ifdef __cplusplus
extern "C"
{
#endif

void push(struct TStack *p, int newId);
int pop(struct TStack *p);

#ifdef __cplusplus
}
#endif

#endif
