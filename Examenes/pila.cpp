#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100

struct TPila {
	int operadores[MAX];
};

double operacion (struct TPila *p){
	int op1,op2;
	op1 = p->operadores[0];
	op2 = p->operadores[1];

	return (op1 + op2);
}

void pregunta (struct TPila *ps){
	int op1, op2;
	printf("operador1: ");
	scanf("%i", &op1);
	ps->operadores[0] = op1;
	printf("operador2: ");
	scanf("%i", &op2);
	ps->operadores[1] = op2;
}

int main(int argc, char *argv[]){

	struct TPila pila;
	int nuevo;


	pregunta(&pila);
	printf("%lf", operacion(&pila));

	return EXIT_SUCCESS;
}
