/*Control a Thread Using Condition Variable*/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

int thread_flag;
pthread_cond_t thread_flag_cv;
pthread_mutex_t thread_flag_mutex;
int cnt=0;

void do_work() {
    cnt++;
    printf("%i\n", cnt);
}

void initialize_flag() {
    pthread_mutex_init (&thread_flag_mutex, NULL);
    pthread_cond_init (&thread_flag_cv, NULL);
    thread_flag = 0;
}

void *thread_function(void *thread_args){
    while(1){
        pthread_mutex_lock (&thread_flag_mutex);
        while(!thread_flag)
          pthread_cond_wait (&thread_flag_cv, &thread_flag_mutex);  /*Con el wait tambien se sale del mutex*/
        pthread_mutex_unlock (&thread_flag_mutex);
        do_work();
    }
    return NULL;
}

void set_thread_flag (int flag_value){
    pthread_mutex_lock (&thread_flag_mutex);
    thread_flag = flag_value;
    pthread_mutex_unlock(&thread_flag_mutex);
}

int main(){

    pthread_t thread_id;
    initialize_flag();

    set_thread_flag(5);

    pthread_create(&thread_id, NULL, &thread_function, NULL);

    pthread_join(thread_id, NULL);


    return EXIT_SUCCESS;
}
