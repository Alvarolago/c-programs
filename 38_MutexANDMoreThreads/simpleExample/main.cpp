#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "stack.h"

void* thread_function(void* args){

    struct TStack *p = (struct TStack*) args;

    printf("%i",p->data[0]);


    return NULL;
}


int main(){

    struct TStack thread_args;

    pthread_t thread_id;

    thread_args.data[0] = 1;

    pthread_create(&thread_id, NULL, &thread_function, &thread_args);


    pthread_join(thread_id,NULL);


    return EXIT_SUCCESS;
}
