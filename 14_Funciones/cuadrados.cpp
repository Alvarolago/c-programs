#include <stdio.h>
#include <stdlib.h>

#define N 10

int cuadrado(int n){
    return n * n;
}

void rellena (int l[N]) {
    for (int n=0; n<N; n++)
        l[n] = cuadrado (n);
}

int  main(){
    int q[N];

    rellena (q);

    for (int n=0; n<N; n++)
        printf ("%i ", q[n]);

    printf ("\n");

    return EXIT_SUCCESS;
}
