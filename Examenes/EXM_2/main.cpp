#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#define NUM_SIGNALS 3

const char *program = "./son";

int spawn () {
    pid_t child_pid;
    child_pid = fork ();  /*Creamos el fork para dividir el programa en 2 procesos
                           iguales con distinto valor de retorno*/
    if(child_pid !=0)
        return child_pid;
    //ELSE
     execvp (program, NULL);  /*Arrancamos el programa hijo del otro archivo "hijo.cpp"*/
     fprintf (stderr, "Un error ocurrio en execvp\n");
     abort ();

}

int main ()
{
    pid_t child_pid;

    int child_status;

    child_pid = spawn ();   /*Guardamos el identificador del hijo
                            que retorna spawn y a la vez llamamos a la función*/
    for(int cnt = 0; cnt < NUM_SIGNALS; cnt++){   /*Mandamos 3 señales con un delay de 1 segundo*/
        sleep(1);
        kill(child_pid, SIGUSR1);
        printf("%i Señales Enviadas\n", cnt);
    }

    wait(&child_status);    /*Esperamos a que el hijo acabe para posteriormente
                            limpiarlo y evitar que se quede Zombie*/

    if(!WIFEXITED (child_status))
        printf ("El proceso hijo no ha salido bien\n");

    return EXIT_SUCCESS;
}

