#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

#define NDEBUG

#ifndef NDEBUG
#define DEBUG(...) fprintf (stderr, __VA_ARGS__);
#else
#define DEBUG(...) ;
#endif


struct TLista {
    char **data;
    int summit;
} lista;

void push(const char *fn_name){
    lista.data = (char **) realloc (lista.data, (lista.summit + 1) * sizeof (char **));
    lista.data[lista.summit++] = strdup (fn_name);
}




void heed_the_call(void *handle){

    const char **(*catalogo)();

    catalogo = ( const char **(*)() ) dlsym( handle, "catalogo" );
    const char **devuelto = (*catalogo)();

    for(const char **nombre = devuelto; *nombre != (char *) 0; nombre++) {
        DEBUG ( "Adding function %s\n", *nombre );
        push( *nombre);
    }
}

void show_functions(){
    printf("REGISTERED FUNCTIONS\n");
    printf("\n");
    for(int cnt=0; cnt<lista.summit; cnt++)
        printf("%s\n", lista.data[cnt]);
    printf("\n");
}




int main(){
	void *handle;

	handle = dlopen ("./libareas.so", RTLD_LAZY);

	if(handle){
            heed_the_call(handle);
            dlclose(handle);
	}
        else{ fprintf(stderr, "Couldnt findo module"); };



        show_functions();


	return EXIT_SUCCESS;
}

// gcc -o aritm aritmetica.o -ldl
