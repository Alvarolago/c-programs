#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>

#define MAX 0x100
#define MAX_SHOP 10
#define FICHERO "file.txt"

FILE *pf = NULL;


struct Queue {        /*Creamos una cola*/
    int summit = 0;
    int data[MAX];
    struct Queue* next;   /*Puntero a la próxima estructura(Esto diferencia Cola de Pila)*/
};

/*VARIABLES SEMAFORO
------------------*/
struct Queue* job_queue;
pthread_mutex_t job_queue_mutex = PTHREAD_MUTEX_INITIALIZER;    /*Indicamos el tipo de seáforo que creamos el PTHREAD_MUTEX... es el estándar*/
sem_t job_queue_cont;



void initialize_job_queue(){    /*Inicializamos la cola a 0*/
    job_queue = NULL;
    sem_init (&job_queue_cont,0,0);
}

void init(struct Queue *cola){  /*Inicializamos el numero de la cola a 0*/
    cola->summit = 0;
}


void begin (struct Queue *cola)   /*Función encargada de escribir en el archivo*/
{
    pf = fopen (FICHERO, "w");
    if (!pf) {
        fprintf (stderr, "No hemos encontrado Harrods.\n");
        exit (1);
    }
    init (cola);
    srand (time (NULL));
}


void push(struct Queue *cola, pthread_t newData){
    cola->data[cola->summit++] = newData;
}

void* thread_function(void* args){

    int n_buys = * (int *) args;    /*Parametros del hilo*/

    while(1){                           /*While Infinito*/

        struct Queue* next_job;         /*Puntero a la proxima estructura*/

        sem_wait(&job_queue_cont);      /*Para el semáforo*/
        pthread_mutex_lock(&job_queue_mutex);   /*Bloqueamos el mutex para que pueda realizar su accion*/

        printf("Dentro del mutex");

        /*Comienza Funcionalidad hilo*/
        for (int i=n_buys; i>0; i--) {
            fprintf (pf, "[ID: %lu] Quedan %i compras.\n.", pthread_self (), i);
            fflush (pf);
            usleep (10000);
        }
        /*Acaba funcionalidad hilo*/

        next_job = job_queue;   /*Asignamos el siguiente trabajo de la cola*/ /*En caso de que la cola esté vacía el semáforo hubiese dado valor de retorno automático*/

        job_queue = job_queue->next;    /*Eliminamos el trabajo de la lista*/

        pthread_mutex_unlock(&job_queue_mutex);   /*Desbloquemos el mutex cuando ha finalizado*/
        //process_job (next_job);

        free(next_job);
    }
    return NULL;
}

pthread_t make_client(){
    pthread_t thread_id;

    int *n_buys = (int *) malloc (sizeof (int));
    *n_buys = rand () % MAX_SHOP + 1;

    pthread_create(&thread_id, NULL, thread_function, n_buys);

    return thread_id;
}


int main(){

    struct Queue c;

    begin(&c);
    for(int cnt=0; cnt<5; cnt++){
        push(&c, make_client());
    }

    fclose(pf);

    return EXIT_SUCCESS;
}




