#include <stdio.h>
#include <stdlib.h>
/* Numero e= lim( 1 + 1/n ) ^ n 
   DE FORMA QUE n TIENDE A INFINITO */
/* Sumando 1 / 1! + 1 / 2! + 1 / 3! ... TAMBIÉN SE OPTIENE EL Nº e */

double factorial (int n){
    if(n == 0)    
        return 1;

    else
    return n * factorial (n-1);
}

int main(){

    int n=0;
    double E=0;

    for(n=0; n<10; n++){
        E = E + (1.0 / factorial(n));
    printf("%.10f\n",E);

    }

    return EXIT_SUCCESS;

}

