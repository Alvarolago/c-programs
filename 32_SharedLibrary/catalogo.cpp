#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

#include "aritmetica.h"

int main(){

    
    void* handle;
    handle = dlopen("./laritmetica.so", RTLD_LAZY);
    	if(!handle){
		fprintf(stderr, "%s\n", dlerror());
		return EXIT_FAILURE;
	}
    void (*aritmetica)() = (void (*)()) dlsym(handle, "add");
    (*aritmetica)();
    dlclose(handle);


    return EXIT_SUCCESS;
}

