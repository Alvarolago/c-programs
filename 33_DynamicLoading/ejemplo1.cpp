#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

int main(){
    void * handle = NULL;

    handle = dlopen("lareas.so", RTLD_LAZY);
    if(!handle){ fprintf(stderr, "%s\n", dlerror()); exit(1); }

    double (*cousine)(double) = dlsym(handle, "cos");

    dlclose(handle);

    return EXIT_SUCCESS;
}

