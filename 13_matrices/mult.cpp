#include <stdio.h>
#include <stdlib.h>

#define M 2
#define K 4
#define N 3

/* Función punto de entrada */
int  main(){

    int i, j;
    double A[M][K] = {
        {2, 3, 5, 1},
        {3, 1, 4, 2}
    },
           B[K][N] = {
               {5,  2, 1},
               {3, -7, 2},
               {-4, 5, 1},
               {2, 3, -9}
           },
           C[M][N];

    for (int i=0; i<M; i++)
        for(int j=0; j<N; j++){
            C[i][j] = 0;
            for (int k=0; k<K; k++)
                C[i][j] += A[i][k] * B[k][j];
        }

    printf("|%.2lf  %.2lf   %.2lf|\n", C[0][0], C[0][1], C[0][2]);
    printf("|%.2lf  %.2lf  %.2lf|\n", C[1][0], C[1][1], C[1][2]);


    return EXIT_SUCCESS;
}
