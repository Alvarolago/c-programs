#include <stdio.h>
#include <stdlib.h>

#define MAX 100
#define N 10
#define ARCH "xdxd.txt"


void volcar (char nombre[N][MAX], FILE *pf){
    for(int i=0; i<N; i++)
        fprintf (pf, "%s\n", nombre[i]);

}

int main(int argc, char *argv[]){

    char nombre[N][MAX];
    nombre[N][MAX] = 0;
    FILE *pf;

    for(int n=0; n<N; n++){
        int i = 0;
        printf("Nombre: ");
        scanf("%s", &nombre[n][i]);
        i++;
    }

    if(! (pf = fopen (ARCH, "w"))){
      fprintf(stderr, "No se ha podido abrir el archivo");
      return EXIT_FAILURE;
    }

    volcar(nombre, pf);

    fclose(pf);


    return EXIT_SUCCESS;

}

