#include <stdlib.h>

int main(int argc, char *argv[]){     //atoi pasa de int a String
    if(argc<3)
        abort();

    return atoi(argv[1]) + atoi(argv[2]);     //atoi pasa de int a String
}
