#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define ARCH "espa~nol.words"
#define N 0x100

long random(long longfile){
	int number;

	srand(time(NULL));

	number = 1 + rand() % ((longfile+1) - 1);

	return number;
}

void palabra(int longword,char word){

    for(int cnt2=0; cnt2<longword; cnt2++)
       printf("%c", word[cnt2])

}


int main(int argc, char *argv[]){

    long inicio, fin;
    long longfile;
    FILE *pf;
    char *list;
    long randomnumb = 0; 
    int beginword = 0;
    int endword = 0;
    int longword = 0;
    char word[N];
    int bg = 0;
    //Variables que utilizamos en los while para autoincrementar
    int i=0;
    int x=0;
    int a=0;

    //Abrimos archivo y ponemos un error en caso de que no se pueda abrir
    if( !(pf=fopen(ARCH, "rb")) ){
        fprintf(stderr,"ERROR");
        return EXIT_FAILURE;
    }

    //Medimos el archivo de esta forma
    inicio = ftell(pf);
    fseek(pf,0,SEEK_END);
    fin = ftell(pf);
    rewind(pf);

    longfile = fin - inicio;

    //Metemos el numero random(función de arriba) en una variable
    randomnumb = random(longfile);

    //Volcamos el archivo en el malloc para poder recortar las palabras
    list = (char*) malloc(longfile);
    fread(list ,sizeof(char *), longfile, pf);


    //Este primer while se encarga de llevar el puntero al comienzo de la palabra que vamos a recortar
    while(list[randomnumb+i++] != '>')
	    beginword++;

    bg = randomnumb + beginword + 2;

    //Este segundo while calcula el tamaño de la palabra seleccionada
    while(list[bg+a++] != '>')
	    longword++;

    //Añadimos caracter a caracter de la palabra en un array
    for(int cnt = bg; cnt < bg+longword; cnt++){
	    word[x++] = list[cnt];
    }
    printf("\n");

    //Imprimimos el array donde se encuentra la palabra
/*    for(int cnt2=0; cnt2<longword; cnt2++)
	    printf("%c", word[cnt2]);
*/

    //AHORA COMENZAMOS CON EL DESARROLLO DE LA INTERFAZ DEL AHORCADO

        char arraysec[N];
        int posicion;
        char letra;
        int cont = 5;
        int w=0;
        int lg=0;

        while(word[lg++] != '\0')
                w++;

        //Preguntamos posición y letra y la vamos guardando en el array por oredn
        for(int cnt=0; cnt<N; cnt++){
        printf("Indique la posición y la letra (pos,letra)");
        scanf("%i,%c", &posicion, &letra);

        //Si la posición en ambos arrays tine la misma letra la letra es correcta en caso contrario el usuario pierde una vida
        arraysec[posicion] = letra;
        if(arraysec[posicion] == word[posicion]){
                for(int cnt=0; cnt<w; cnt++)
                        if(arraysec[cnt] == word[cnt])
                                printf(" %c", arraysec[cnt]);
                        else
                                printf(" _");
        printf("\n");
        }
        else if(posicion > w)
                printf("Se ha pasado. La palabra contiene %i letras.\n", w);
        else{
                printf("Perdiste una vida.\n");
                cont--;
                if(cont == 5)
                        printf("Le quedan: ♥ ♥ ♥ ♥ ♥  vidas");
                else if(cont == 4)
                        printf("Le quedan: ♥ ♥ ♥ ♥ _ vidas");
                else if(cont == 3)
                        printf("Le quedan: ♥ ♥ ♥ _ _ vidas");
                else if(cont == 2)
                        printf("Le quedan: ♥ ♥ _ _ _vidas.\n");
                else if(cont == 1)
                        printf("Le quedan: ♥ _ _ _ _ vidas.\n");
                else
                        printf("No le quedan más vidas.\n");
	}
        }

        printf("%s", palabra(longword,word[N]));



        free(list);
        fclose(pf);

    return EXIT_SUCCESS;

}

