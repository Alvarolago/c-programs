#ifndef __GENERAL__H__
#define __GENERAL_H__

#include <stdlib.h>
#include <stdio.h>

#define MAX 0x100
#define N 0x100

struct TEstrella {
        double coordX, coordY;
        double distancia, diametro, masa, edad;
};


struct TPila {
        struct TEstrella *datos[N];
        int cima;
};



#endif
