#ifndef __INTERFAZ_H__
#define __INTERFAZ_H__

#include "stdio.h"
#include "stdlib.h"

enum MenuOption
{
    new_client,
    close_sm,

    TOTAL_MENU_OPT
};

#ifdef __cplusplus
extern "C" {
#endif
     enum MenuOption menu ();
#ifdef __cplusplus
}
#endif

#endif
