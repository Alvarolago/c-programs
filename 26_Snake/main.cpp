#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <ncurses.h>

#include "snake.h"

void pintar(struct TSnake snake) {
    clear();
    for(int i=0; i<snake.cima; i++)
        mvprintw ( snake.anillo[i].pos.y,
                   snake.anillo[i].pos.x, "O" );
    refresh ();
}

int main(int argc, char *argv[]){
    struct TSnake snake;
    int input;
    int posib=1;

    initscr();
    halfdelay(2);
    keypad(stdscr, TRUE);
    iniciar(LINES, COLS);

    parir(&snake);


    do{
        input = getch();
        if(input >= KEY_DOWN && input <= KEY_RIGHT)
            snake.anillo[0].vel = velocidades[input - KEY_DOWN];

        mover(&snake);
        if(snake.anillo[0].pos.x > COLS)
           snake.anillo[0].pos.x = 0;
        if(snake.anillo[0].pos.y > LINES)
            snake.anillo[0].pos.y = 0;
        if(snake.anillo[0].pos.y < 0)
            snake.anillo[0].pos.y = LINES;
        if(snake.anillo[0].pos.x < 0)
            snake.anillo[0].pos.x = COLS;

        pintar(snake);
    }while( input != 0x1B);

    endwin();     //Es como un free de initscr




    return EXIT_SUCCESS;

}

