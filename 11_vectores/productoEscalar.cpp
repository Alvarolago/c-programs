#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define PI 3.14

int main(){

    double vec1[2], vec2[2];
    double total, angulo, producto;
    double modulo1, modulo2;

    system("clear");

    printf("Indique el primer vector: ");
    scanf("%lf,%lf", &vec1[0], &vec1[1]);

    printf("Indique el segundo vector: ");
    scanf("%lf,%lf", &vec2[0], &vec2[1]);

    modulo1 = sqrt( pow(vec1[0],2) + pow(vec1[1],2));
    printf("Modulo 1º vector: %.2f\n", modulo1);

    modulo2 = sqrt( pow(vec2[0],2) + pow(vec2[1],2));
    printf("Modulo 2º vector: %.2f\n", modulo2);

    producto =  (vec1[0]*vec2[0]) + (vec1[1]*vec2[1]);

    total = producto / (modulo1 * modulo2);

    angulo = acos(total) * 180 / PI;

    printf("El angulo formado es %.2lfº\n", angulo);

    return EXIT_SUCCESS;

}

