#include <stdio.h>
#include <stdlib.h>
#define BMP_NAME "tiger.bmp"
#define WIDTH 300L
#define HEIGHT 168L
#define Bpp 3
#define ROW_SIZE (WIDTH * Bpp + 3 ) / 4 * 4

#define IMAGE_OFFSET 0x36

int main(int argc, char *argv[]){

    unsigned char *image;
    image = (unsigned char *)malloc(HEIGHT * ROW_SIZE);	
    //malloc reserva tantos bytes como tu le digas. (unsigned char *)lee de byte en byte a partir del puntero


    FILE *pf;


    if(! (pf = fopen (BMP_NAME, "r"))){
        fprintf(stderr,"Error");
        return EXIT_FAILURE;
    }

    fseek(pf, IMAGE_OFFSET, SEEK_SET);
    fread(image, 1, HEIGHT * ROW_SIZE * Bpp, pf);
    fclose (pf);



    for(int row=HEIGHT; row>=0; row--) {
        for(int col=0; col<WIDTH; col++) {
            double media = 0;
            for(int i=0; i<Bpp; i++)
                media += image[ROW_SIZE * row + Bpp*col+i];
            media /= 3;

	    if (media < 25)
		printf("██");
	    else if (media < 50)
                printf("▓▓");
            else if(media < 75)
                printf("▒▒");
            else if(media <100)
                printf("░░");
            else if (media <150)
                printf("..");
	    else
		printf("  ");
        }
        printf ("\n");
    }

    for(int cnt=0; cnt<IMG_OFFSET; cnt++){
	    fseek(pf, 0, SEEK_SET);
	    fread(image,1 , HEIGHT * ROW_SIZE * Bpp, pf);
	    printf()
    }



    free(image); //Libera la memoria del malloc

    return EXIT_SUCCESS;

}

