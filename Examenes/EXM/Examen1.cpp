#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include "extend.h"

#define FILENAME "fichero.txt"
#define MAX 0x100

#define N 0x100 		//Cantidad de estrellas que caben





struct TEstrella {
	double coordX, coordY;
	double distancia, diametro, masa, edad;
};


int menu(){
	char respuesta;
	printf("Continuar?");
	scanf("%c",&respuesta);
	if(respuesta == 's')
		return 1;
	else{
		if(respuesta == 'n')
			return 0;
	}
}


struct TPila {
	struct TEstrella *datos[N];
	int cima;
};



void push (struct TPila *p, struct TEstrella *nuevo){
	p->datos[p->cima] = nuevo;
	p->cima++;
}



int main(int argc, char *argv[]){

    struct TEstrella *estrella;
    struct TPila *stack = NULL;

    FILE *pf;
    char respuesta;
    int seguir;
 
    bzero (&stack, sizeof(stack));

	if( !(pf = fopen(FILENAME, "w")) ){
        	fprintf(stderr, "Error No es posible abrir el archivo indicado\n");
	        return EXIT_FAILURE;
    	}
	
	preguntar(stack, estrella, pf);

	fclose(pf);  

    return EXIT_SUCCESS;

}

