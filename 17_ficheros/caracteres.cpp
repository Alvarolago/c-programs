#include <stdio.h>
#include <stdlib.h>

#define FILENAME "text.txt"

int main(int argc, char *argv[]){

    FILE *pf;

    if( !(pf = fopen(FILENAME, "r")) ){
        fprintf(stderr, "Error No es posible abrir el archivo indicado\n");
        return EXIT_FAILURE;
    }

    ftell(pf);
    printf("%i Bytes\n", (char) getc(pf));


    fclose(pf);

    return EXIT_SUCCESS;

}

