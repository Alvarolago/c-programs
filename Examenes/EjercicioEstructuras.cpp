#include <stdio.h>
#include <stdlib.h>

#define MAX 0x50
#define N 3

struct TAlumno {
    char Nombre[MAX];
    char Apellido[MAX];
    double Asignaturas[N][MAX];
};

void rellenar(struct TAlumno *p){

    int asignatura;
    double nota;

    printf("Nombre Alumno: ");
    scanf("%s", p->Nombre);
    printf("Apellido Alumno: ");
    scanf("%s", p->Apellido);
    printf("Asignatura: ");
    scanf("%i", &asignatura);
    printf("Nota del Alumno: ");
    scanf("%lf", &nota);
}

void imprimir(struct TAlumno *p){
	printf("%s", p->Nombre);
	printf("%s", p->Apellido);
	printf("%lf", p->Asignaturas[0][0]);
}

int main(int argc, char *argv[]){
    struct TAlumno pila;

    rellenar(&pila);
    imprimir(&pila);


    return EXIT_SUCCESS;

}

