#include <stdio.h>
#include <stdlib.h>


float valor_e(int e){
     float f, E = 1.0;
      for (int i=1; i<=e; i++){
          f = 1;
          for (int j=1; j<=i; j++){
              f = f * j; // DE AQUI SACAMOS EL FACTORIAL
          }
          E = E + 1.0/f;
      }
      return E;
  }


int main(int argc, char *argv[]) {

     int e;

     printf("Cuanto te quieres acercar al valor de E: ");
     scanf(" %i", &e);

     valor_e(e);

     printf ("Valor de E es %.15000lf\n", valor_e(e));

    return EXIT_SUCCESS;
}
