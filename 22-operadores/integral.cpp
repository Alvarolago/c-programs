#include <stdio.h>
#include <stdlib.h>


#define INC 0.0001
double parabola (double x) {return x * x; }

double integral (
        double  li, double ls,
        double (*pf)(double) )    //Le das un double a la funcion *pf y devuelve un double
{
    double area = 0;
    for(double x=li; x<ls; x+=INC)
        area += INC * (*pf)(x);
    return area;

}

int main(int argc, char *argv[]){

    long li, ls;

    printf("Indique el límite inferior");
    scanf("%lf", li);
    printf("Indique el límite superior");
    scanf("%lf", ls);

    printf("El area es: %lf\n", integral (li, ls, &parabola));





    return EXIT_SUCCESS;

}


//http://www.poleyland.com/hp48/


