#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>


int spawn(char* program, char **arg_list){
    pid_t child_pit;

    child_pit = fork();
    if(child_pit != 0){
        return child_pit;
    }else{
        execvp(program, arg_list);
        printf("Error");
        abort();
    }
}



int main (){
    char* arg_list[] = {
        "ls",
        "-l",
        "/",
        NULL
    }

    spawn("ls", arg_list);


    printf("done with main program");

    return 0;
}
