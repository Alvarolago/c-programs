#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>

#define MAX 10
#define T_LADRILLOS 600

const char *nombre_obreros[2] = {"Mariano", "Benito"};
int recogidos = 0;
int ladrillos = 0;

pthread_mutex_t mutex_quitar = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_coger = PTHREAD_MUTEX_INITIALIZER;
sem_t semaph;
                              /*Capataz tiene 2 funciones, decide de forma random
                              si el obrero coge 1 o 2 ladrillos y la otra avisar al
                              último obrero de cuantos ladrillos faltan*/
int capataz(){
    if(ladrillos == (T_LADRILLOS - 1))
        return 1;

    return (rand() %2) + 1;
}

void mostrar(int nam) {   /*Mostramos por terminal como va la cosa*/
    printf("Soy: %s , con ID: %li y llevamos recogido %i , quedan , quedan ladrillos disponibles %i\n",
                 nombre_obreros[nam] ,pthread_self() , recogidos, ladrillos);
}

void quitar(int pLadrillos) {
     pthread_mutex_lock(&mutex_quitar);
     ladrillos -= pLadrillos;
     pthread_mutex_unlock(&mutex_quitar);
}

void coger(int pLadrillos) {
     pthread_mutex_lock(&mutex_coger);
     recogidos += pLadrillos;
     pthread_mutex_unlock(&mutex_coger);
}

void recogerLadrillos(int pLadrillos){
    if(ladrillos > 0){
        quitar(pLadrillos);
        coger(pLadrillos);
    }
}

void visitar(int n){
    int lad = 0;
    while(recogidos < T_LADRILLOS){
        sem_wait(&semaph);    /*Limitamos el acceso con el semáforo*/
        lad = capataz();
        recogerLadrillos(lad);
        mostrar(n);
        sem_post(&semaph);    /*Liberamos en 1 los accesos*/
        usleep(100000);
   }
}

void* function_obrero1(void* args){
    const char *nombre = (const char *) args;

    visitar(0);   /*Ponemos dentro de visitar el numero que nos servirá
                  para asociar el correspondiente nombre del obrero*/
    return NULL;
}


void* function_obrero2(void *args){
    const char * nombre = (const char *) args;

    visitar(1);

    return NULL;
}

void* function_fabrica(void * args){
    int contador=0;

    while(contador < T_LADRILLOS){  /*Fabrica 600 ladrillos*/
      ladrillos++;
      contador++;
      usleep(10000);
    }
    printf("La fabrica cerró\n");

    return NULL;
}


int main(){

    sem_init(&semaph, 0, 1);    /*Inicializamos el semáforo*/
    srand(time(NULL));

    pthread_t thread1_id;
    pthread_t thread2_id;
    pthread_t fabrica_id;

    /*Creamos los hilos*/
    pthread_create(&thread1_id, NULL, &function_obrero1, (void*) "Mariano");
    pthread_create(&thread2_id, NULL, &function_obrero2, (void*) "Benito");
    pthread_create(&fabrica_id, NULL, &function_fabrica, NULL);

    /*Cerramos los hijo al acabar*/
    pthread_join(thread1_id, NULL);
    pthread_join(thread2_id, NULL);
    pthread_join(fabrica_id, NULL);

    sem_destroy(&semaph);   /*Limpiamos de memoria el semáforo*/

    return EXIT_SUCCESS;
}
