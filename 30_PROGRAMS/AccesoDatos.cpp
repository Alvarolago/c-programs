#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>


#define FILENAME "buffer.txt"


struct TAlumno{
	char nombre[40];
	int edad;
};


void menu() {
	printf("-----------------------------\n");
	printf("1. Cargar datos de un fichero\n");
	printf("2. Guardar datos en tabla\n");
	printf("3. Escribir tabla en pantalla\n");
	printf("4. Insertar datos en tabla\n");
	printf("5. Borrar dato de tabla\n");
	printf("6. Salir\n");
	printf("-----------------------------\n");
	printf("Introduce la opcion: ");
}



 cargarDatos(struct TAlumno tablaAlumnos[10]){
	FILE * pf;
	char nombreAlumno[40];
	int edadAlumno,	posicion = 0;

    if( !(pf = fopen (FILENAME, "rt")))
        return EXIT_FAILURE;

	
	if (pf != NULL)
	{

			scanf("%s", nombreAlumno);
			scanf("%i", &edadAlumno);
			strcpy(tablaAlumnos[posicion].nombre, nombreAlumno);
			tablaAlumnos[posicion].edad = edadAlumno;
			posicion++;

	}
	printf("%s", tablaAlumnos[0].nombre);
}

void escribirDatos(struct TAlumno tablaAlumnos[10]){

	FILE *pf;
	int numeroNombres = 0;
	
	if( !(pf = fopen (FILENAME, "wt"))){
		fprintf(stderr,"Error");
        //return EXIT_FAILURE;
    }
        
	for(int i=0; i<10; i++){
		if(tablaAlumnos[i].nombre == '\0'){
			numeroNombres = i;
			while(i<10){
				fprintf(pf, "%s", tablaAlumnos[i].nombre);
				//fprintf(pf, "%i", edad);
			}
		}
	}
	printf("%i\n", numeroNombres);
	
				
}





void entrada(){
	
	struct TAlumno Alumno[10];
	int opcion;
	
	do{
		menu();
		scanf("%i", &opcion);
		switch(opcion){
			case 1: cargarDatos(Alumno);
			break;
			case 2: escribirDatos(Alumno);
			break;
			/*
			case 3: insertarDatos(Alumno);
			break;
			*/
		}
	}while(opcion != 6);
}



int main(){
	
	entrada();
	
	
	
	return EXIT_SUCCESS;
}
