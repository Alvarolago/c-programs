
/* Curve constructor */
function Curve(p){
    this.p = [] /* List of points */
    if (typeof(p) !== "undefined")
        this.p = p
    this.d = [] /* Displacement between points */
    this.s = [] /* Space between points */
    this.u = [] /* Unit displacement vector */
    this.scale = 1
}


/* digest a list of points and store results */
Curve.prototype.digest = function () {
    for (var i=0; i<this.p.length-1; i++ ){
        this.d[i] = [ this.p[i+1][0] - this.p[i][0],
                      this.p[i+1][1] - this.p[i][1] ]
        this.s[i] = module(this.d[i])
        this.u[i] = [this.d[i][0] / this.s[i], this.d[i][1] / this.s[i]]
    }
}

Curve.prototype.transform = function (field) {
    var result = []
    for (var i=0; i<this[field].length; i++)
        result.push( [ this[field][i][0] * this.scale, this[field][i][1] * this.scale ] )

    return result
}
