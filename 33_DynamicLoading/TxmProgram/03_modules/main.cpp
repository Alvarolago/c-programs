#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

/* run: ./lista modules.cfg */

#define NDEBUG

#ifndef NDEBUG
#define DEBUG(...) fprintf (stderr, __VA_ARGS__);
#else
#define DEBUG(...) ;
#endif

char *progname;

struct TLista {
    char **data;
    int summit;
} lista;

void push(const char *fn_name){
    lista.data = (char **) realloc ( lista.data, (lista.summit + 1) * sizeof (char **));
    lista.data[lista.summit++] = strdup (fn_name);
}

void
print_usage (FILE *out)
{
    fprintf (out,
"\n"
"\n%s <config_file>\t - Loads modules from config file and prints function names contained within."
"\n\n", progname);
}

void
die (const char *mssg, int retcode)
{
    fprintf (stderr, "%s\n", mssg);
    print_usage (stderr);

    if (retcode)
        exit (retcode);
}

void heed_the_call(void *handle)      //Genera una pila con todos los nombres del catalogo
{
    const char **(*catalogo)();
    char buffer[0x100];     //Puntero a una funcion

    catalogo = ( const char **(*)() ) dlsym( handle, "catalogo" );    //dlsym --> Das nombre y te dice la Direccion de memoria donde se encuentra la funcion catalogo en este caso
    const char **devuelto = (*catalogo)();      //(*catalogo)() los 2º parentesis indica que lo ejecute Direccion de memoria + () significa que se ejecute alla donde apunta y lo guarda en devuelto
//Devuelto contiene la direccion de la 1º palabra

    for (const char **nombre = devuelto; *nombre != (char *) 0; nombre++) {     //Va recorriendo nombre a nombre para tener la lista de palabras que indican los nombres de las funciones
        //DEBUG ( "Adding function %s\n", *nombre );      //MACRO para ver como va el programa
        push (*nombre);     //Empuja la palabra a una pila
    }

}

void
show_functions()
{
    printf ("REGISTERED FUNCTIONS\n");
    printf ("====================\n");
    printf ("\n");
    for (int i=0; i<lista.summit; i++)
        printf ("%s\n", lista.data[i]);
    printf ("\n");
}

void
deliver()   //Liberar la pila
{
    for (int i=0; i<lista.summit; i++)
        free (lista.data[i]);
    free (lista.data);
}

int
main(int argc, char *argv[])
{
    void *handle = NULL;
    FILE *module = NULL;
    char modname[0x100];

    progname = argv[0];

    if (argc<2)
        die("Not enough arguments.", 1);

    if ( !(module = fopen(argv[1], "r") ))
            die ("Config file not found.", 2);

    while ( !feof(module) )     //feof funcion que devuelve true si llega hasta el final del fichero
        if (fscanf (module," %s", modname) != EOF){   //modname buffer, lee caracter a caracter
             DEBUG ( "Opened %s\n", modname );
             handle = dlopen (modname, RTLD_LAZY);    //RTLD_LAZY carga en la memoria solo cuando lo abre

             if (handle) {
               heed_the_call(handle);   //atender a la llamada
               dlclose (handle);
             } else
                fprintf (stderr, "Couldn't find module: %s\n", modname);
        }

    show_functions ();
    deliver ();

    return 0;
    }
