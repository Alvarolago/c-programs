#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 7

int min(int A[], int *mayor) {
	int p_min = 0;
	while (A[p_min] <= *mayor)
		p_min++;
	for(int i=p_min + 1; i<N; i++)
		if(A[i] < A[p_min] && A[i] > *mayor)
			p_min = i;
	*mayor = A[p_min];
	return A[p_min];
}



int main(int argc, char *argv[]){

	int A[N];
	int B[N];
	int mactual = -1;

	for(int n=0; n<N; n++){
		printf("Indique el número: ");
		scanf("%i", &A[n]);
	}
	for(int r=0; r<N; r++)
		B[r] = min(A, &mactual);
	memcpy(A, B, sizeof (A) );

	for(int i=0; i<N; i++)
		printf("%i", A[i]);
	printf("\n");


    return EXIT_SUCCESS;

}

