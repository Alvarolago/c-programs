#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

int cnt;
int thread_flag;
pthread_cond_t thread_flag_cv;
pthread_mutex_t thread_flag_mutex;

void init () {
    pthread_mutex_init (&thread_flag_mutex, NULL);
    pthread_cond_init  (&thread_flag_cv, NULL);
    thread_flag = 0;
    cnt=0;
}

void do_work() {
    printf("%i\n", cnt-1);
    thread_flag = 0;         /*Volvemos a bajar la bandera*/
}

void set_pthread_flag (int flag_value) {      /*Ponemos un valor a la bandera*/
    pthread_mutex_lock (&thread_flag_mutex);
      thread_flag = flag_value;
      pthread_cond_signal (&thread_flag_cv);
    pthread_mutex_unlock (&thread_flag_mutex);
}

void *thread_func2 (void *args) {     /*2º función se encarga de comprobar todo el rato si ha recibido señal o no*/
    while(1){
        pthread_mutex_lock (&thread_flag_mutex);
        while(!thread_flag)
            pthread_cond_wait (&thread_flag_cv, &thread_flag_mutex);

        pthread_mutex_unlock (&thread_flag_mutex);
        do_work();                  /*Si recibe señal sale del mutex y ejecuta do_work()*/
    }
    return NULL;
}


void *thread_func (void *args){
    while(1){
       sleep(1);
       if(cnt%5==0)           /*Cada 5 seg cambia la bandera*/
           set_pthread_flag (1);
       cnt++;
   }
    return NULL;
}

int main() {

    pthread_t time_id;
    pthread_t hd_id;

    init();

    pthread_create(&time_id, NULL, &thread_func, NULL);
    pthread_create(&hd_id, NULL, &thread_func2, NULL);


    pthread_join (time_id, NULL);
    pthread_join (hd_id, NULL);



    return EXIT_SUCCESS;
}
