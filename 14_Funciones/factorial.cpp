#include <stdio.h>
#include <stdlib.h>

int factor (int numero, int profundidad){


    if(numero == 1)
        return numero;
    return numero + 1./factor(numero,profundidad-1);

}


int main(){

    int numero;
    int profundidad;
    double res;

    printf("Indique numero: ");
    scanf("%i", &numero);
    printf("Indique la profundidad: ");
    scanf("%i", &profundidad);

    res = factor(numero, profundidad);
    printf("%lf",res);

    return EXIT_SUCCESS;

}

