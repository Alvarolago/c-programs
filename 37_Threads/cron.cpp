#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

struct TParams{
    char data[100];
    int summit = 0;
};

void* thread_function(void* parametres){
    struct TParams* p = (struct TParams*) parametres;

    for(int cnt=0; cnt<=60; cnt++){
      printf("%c", p->data[p->summit]);
      sleep(1);
    }

    return NULL;
}


int main(){

    pthread_t thread_id;
    pthread_t thread2_id;

    struct TParams thread_args;
    struct TParams thread2_args;

    for(int cnt=0; cnt<10; cnt++)
      thread_args.data[cnt] = 'a';

    pthread_create(&thread_id, NULL, &thread_function, &thread_args);
    pthread_create(&thread2_id, NULL, &thread_function, &thread_args);

    pthread_join(thread_id, NULL);
    pthread_join(thread2_id, NULL);


    return EXIT_SUCCESS;
}
