#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <time.h>

#define RED_ON   "\x1B[31m"
#define GREEN_ON "\x1B[32m"
#define BLUE_ON  "\x1B[34m"
#define CYAN_ON  "\x1B[36m"
#define RESET    "\x1B[0m"

#define MAX 0x100
#define LMUERTE 10

#define ARCH "espa~nol.words"

//Comprueba las letras
bool es_mala (char letra) {
    return true;
}

//Busca numero random
long random(long longfile){
	int number;

	srand(time(NULL));

	number = 1 + rand() % ((longfile+1) - 1);

	return number;
}

//Calcula el tamaño del archivo
long sizefile(FILE *pf){

	long longfile;
	long inicio,fin;

    inicio = ftell(pf);
    fseek(pf,0,SEEK_END);
    fin = ftell(pf);
    rewind(pf);

    longfile = fin - inicio;

    return longfile;
}


int  main(int argc, char *argv[]){
    char letra;
    char *malas, *letras;
    int total_malas = 0, total_letras = 0;
    long longfile;
    long randomnumb;

//Almacenamiento de las letras
    malas = (char *) malloc (LMUERTE + 1);
    letras = (char *) malloc (MAX + 1);
    bzero (malas, LMUERTE + 1);
    bzero (letras, MAX + 1);

	FILE *pf;
	
	if(!(pf=fopen(ARCH, "rb")) ){
		fprintf(stderr,"Can not open the file");
		return EXIT_FAILURE;
	}

	longfile = sizefile(pf);
	randomnumb = random(longfile);
	printf("%li ", longfile);
	printf("%li ", randomnumb);

   char *list;
   int i=0,a=0,x=0;
   int beginword, bg, longword;
   char word[0x20];

    list = (char*) malloc(longfile);
    fread(list ,sizeof(char *), longfile, pf);

    while(list[randomnumb + i++] != '\n')
	    beginword++;

    bg = randomnumb + beginword + 2;

    //Este segundo while calcula el tamaño de la palabra seleccionada
    while(list[bg + a++] != '\n')
	    longword++;

    //Añadimos caracter a caracter de la palabra en un array
    for(int cnt = bg; cnt < bg+longword; cnt++){
	    word[x++] = list[cnt];
    }

    for(int cnt2=0; cnt2<longword; cnt2++)
	    printf("%c", word[cnt2]);




// Menú
    while (total_malas < LMUERTE){
        system ("clear");
        system ("toilet -f pagga AHOGADO");
        printf ("\n");
        printf ("Letras: " GREEN_ON "%s" RESET "\n", letras);
        printf ("Malas: " RED_ON "%s" RESET "\n", malas);
        printf ("=============================\n\n");

        printf (CYAN_ON "Letra: ");
        letra = getchar ();
        __fpurge (stdin);
        printf ( RESET "\n" );

        if (strchr (letras, letra))
            continue;

        if (es_mala (letra))
            *(malas + total_malas++) = letra;
	else{
        *(letras + total_letras++) = letra;

	int x=0;
	if(letra == word[x++]){
		for(int cnt=0; cnt<longword; cnt++)
			if(letra == word[cnt])
				printf(" %c", word[cnt]);
			else
				printf(" _");

    		}	
	}
    }

    free (letras);

    free (malas);

    return EXIT_SUCCESS;
}
