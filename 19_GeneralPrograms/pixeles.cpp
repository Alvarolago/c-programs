#include <stdio.h>
#include <stdlib.h>

#define ARCHIVO "leon.bmp"
#define N  700
#define MAX 390

int main(int argc, char *argv[]){

    FILE *fp;

    char array[N][MAX];

	    if(! (fp = fopen (ARCHIVO, "r")) ){
		    fprintf (stderr, "No he podido abrir fichero.\n");
	    	return EXIT_FAILURE;
    		}
 	
	    fseek ( fp, 0, SEEK_SET);

	    for(int i=0; i<N; i++)
	    fgetc(fp);
	    fread(array,2,2,fp);

	    for(int i=0; i<N; i++)
	    printf("Fila %i: %s\n",i,(char) array[i]);

	    fclose(fp);

    return EXIT_SUCCESS;

}

